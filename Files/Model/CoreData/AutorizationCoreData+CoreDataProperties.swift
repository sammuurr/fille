//
//  AutorizationCoreData+CoreDataProperties.swift
//  Files
//
//  Created by ADMIMN on 21.01.2021.
//
//

import Foundation
import CoreData


extension AutorizationCoreData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AutorizationCoreData> {
        return NSFetchRequest<AutorizationCoreData>(entityName: "AutorizationCoreData")
    }

    @NSManaged public var accessToken: String?
    @NSManaged public var tokenType: String?
    @NSManaged public var refreshToken: Date?
    @NSManaged public var tokenTimeOut: Date?
    @NSManaged public var refreshTokenTimeOut: Date?

}

extension AutorizationCoreData : Identifiable {

}
