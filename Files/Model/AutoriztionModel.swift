//
//  AutoriztionModel.swift
//  Files
//
//  Created by ADMIMN on 20.01.2021.
//

import Foundation
import UIKit
import CoreData

struct Autorization: Decodable{
    
    let token_type: String
    let access_token: String
    let expires_in: String
    let refresh_token: String
    let refresh_token_expires_in: String
    
}

class AutoriztionModel {
    
    let container = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    let viewContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func sigin(login:String, password: String) -> Autorization? {
        
        var auturization: Autorization?
        let semaphore = DispatchSemaphore (value: 0)

        
        let parameters = "login=\(login)&password=\(password)"
        let postData =  parameters.data(using: .utf8)
        

        var request = URLRequest(url: URL(string: "http://macbook-pro-admimn.local:3000/api/auth/sign_in")!)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {
                
                print(String(describing: error))
                semaphore.signal()
                return
    
            }
            
            auturization = try? JSONDecoder().decode(Autorization.self, from: data)
            
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
        saveCoreData(auturization: auturization)
 
        return auturization
        
    }
    
    func auth(refreshtoken:String) -> Autorization? {
        
        var auturization: Autorization?
        let semaphore = DispatchSemaphore (value: 0)

        
        let parameters = "refresh_token=\(refreshtoken)"
        let postData =  parameters.data(using: .utf8)
        

        var request = URLRequest(url: URL(string: "http://macbook-pro-admimn.local:3000/api/auth/refresh?refresh_token=2RHaY3NKQeMGJxnQZQ9SH6RUpsr1Cro18yPtUqemh8VnJxeS8L4n1kMrNvEfuShU")!)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {
                
                print(String(describing: error))
                semaphore.signal()
                return
    
            }
            
            auturization = try? JSONDecoder().decode(Autorization.self, from: data)
            
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
        saveCoreData(auturization: auturization)
 
        return auturization
        
    }
    
    func saveCoreData(auturization:Autorization?) {
        DispatchQueue.global(qos: .default).async { [self] in
            let entityCount:Int? = try? viewContext.count(for: NSFetchRequest<AutorizationCoreData>(entityName: "AutorizationCoreData"))
                        
            if (entityCount != nil){
                if (entityCount! <= 10){
                    if auturization != nil{
                        let item = AutorizationCoreData(context: viewContext)
                        
                        item.tokenType = auturization!.token_type
                        item.accessToken = auturization!.access_token
                        item.refreshToken = auturization!.refresh_token
                        
                        let formater = ISO8601DateFormatter()
                        
                        formater.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
                        
                        
                        item.tokenTimeOut = formater.date(from: auturization!.expires_in)
                        item.refreshTokenTimeOut = formater.date(from: auturization!.refresh_token_expires_in)
                        
                        let a = try? viewContext.save()
                        
                    }
                }else{
                    
                    
                    let fetch = NSFetchRequest<AutorizationCoreData>(entityName: "AutorizationCoreData")
                    let sortInfo = NSSortDescriptor(key: "tokenTimeOut", ascending: false)
                    fetch.sortDescriptors = [sortInfo]
                    
                    let fetchResult = try? viewContext.fetch(fetch)
                    var counter = entityCount! - 1
                    
                    guard fetchResult != nil else { return }
                    
                    while entityCount! > 7{
                        if counter < 8 { break }
                        
                        viewContext.delete(fetchResult![counter])
                        
                        counter -= 1
                    }
                    
                    try? viewContext.save()
                    
                    if auturization != nil{
                        let item = AutorizationCoreData(context: viewContext)
                        
                        item.tokenType = auturization!.token_type
                        item.accessToken = auturization!.access_token
                        item.refreshToken = auturization!.refresh_token
                        
                        let formater = ISO8601DateFormatter()
                        
                        formater.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
                        
                        
                        item.tokenTimeOut = formater.date(from: auturization!.expires_in)
                        item.refreshTokenTimeOut = formater.date(from: auturization!.refresh_token_expires_in)
                        
                        let a = try? viewContext.save()
                        print(a)
                    }
                }
            }
        }
    }
    
}
