//
//  Groups.swift
//  Files
//
//  Created by ADMIMN on 21.01.2021.
//

import Foundation
import UIKit
import CoreData

struct GroupsModel: Decodable{
    
    let id: String
    let name: String
    let parent_folder_id: String?
    let children_exist: Bool
    
}


class Groups {
    
    let container = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    let viewContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let autorizationClass = AutoriztionModel()
    
    func fetchGroups(auth:Autorization?, folder_id:String?) -> [GroupsModel]? {
        
        let semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: "http://macbook-pro-admimn.local:3000/api/v1/testings/folders?")!)

        if folder_id != nil, folder_id != ""{
            request.url = URL(string: "http://macbook-pro-admimn.local:3000/api/v1/testings/folders?folder_id=\(folder_id!)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!
            
        }
        
        var groups: [GroupsModel]?

        
        let fetch = NSFetchRequest<AutorizationCoreData>(entityName: "AutorizationCoreData")
        let sortInfo = NSSortDescriptor(key: "tokenTimeOut", ascending: false)
        fetch.sortDescriptors = [sortInfo]
        
        let fetchResult = try? viewContext.fetch(fetch)
        
        
        
        if auth == nil {
            
            //сюда добавить проверку на нил fetchResult![0].accessToken! если нил то отправтить авторизоваться заново

            request.addValue("Bearer \(fetchResult![0].accessToken!)", forHTTPHeaderField: "Authorization")
            request.httpMethod = "GET"
            
        }else{
            request.addValue("Bearer \(auth!.access_token)", forHTTPHeaderField: "Authorization")
            request.httpMethod = "GET"
        }
    
        

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
            }
            
            groups = try? JSONDecoder().decode([GroupsModel].self, from: data)
            print(groups)
            
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
        if groups == nil{
            groups = reFetchGroups(fetchResult: fetchResult, folder_id: folder_id)
        }
        return groups

    }
    
    private func reFetchGroups(fetchResult:[AutorizationCoreData]?, folder_id:String?) -> [GroupsModel]?{
        let token = fetchResult?[0].refreshToken
        
        if token != nil {
            
            let auth = autorizationClass.auth(refreshtoken: token!)
            let token = auth?.access_token
            var groups: [GroupsModel]?
            
            if token != nil{
                
                let semaphore = DispatchSemaphore (value: 0)

                
                var request = URLRequest(url: URL(string: "http://macbook-pro-admimn.local:3000/api/v1/testings/folders")!)
                
                if folder_id != nil{
                    request.url = URL(string: "http://macbook-pro-admimn.local:3000/api/v1/testings/folders?folder_id=\(folder_id!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))")
                    
                }
                
                request.addValue("Bearer \(token!)", forHTTPHeaderField: "Authorization")
                request.httpMethod = "GET"

                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data else {
                        print(String(describing: error))
                        semaphore.signal()
                        return
                    }
                    
                    groups = try? JSONDecoder().decode([GroupsModel].self, from: data)
                    
                    semaphore.signal()
                }

                task.resume()
                semaphore.wait()
                
                return groups
                
            }
        }
        
        return nil

    }
    
    func addNewGroup(name:String, place:String) -> GroupsModel? {
        
        var group:GroupsModel?
        let semaphore = DispatchSemaphore (value: 0)

        let parameters = "parent_folder_id=\(place)&name=\(name)"
        let postData =  parameters.data(using: .utf8)

        
        var request = URLRequest(url: URL(string: "http://macbook-pro-admimn.local:3000/api/v1/testings/folders")!,timeoutInterval: Double.infinity)
        
        let fetch = NSFetchRequest<AutorizationCoreData>(entityName: "AutorizationCoreData")
        let sortInfo = NSSortDescriptor(key: "tokenTimeOut", ascending: false)
        fetch.sortDescriptors = [sortInfo]
        let fetchResult = try? viewContext.fetch(fetch)
        
        
        if (auth?.access_token) != nil{
            request.addValue("Bearer \(auth!.access_token)", forHTTPHeaderField: "Authorization")
        }else{
            request.addValue("Bearer \(fetchResult![0].accessToken!)", forHTTPHeaderField: "Authorization")
        }
        
        request.addValue("application/x-www-form--urlencoded", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            
            group = try? JSONDecoder().decode(GroupsModel.self, from: data)
            print(group)
            
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
        if group == nil, fetchResult != nil{
            
            let reAdded = reAddNewGroup(name: name, place: place, fetchResult: fetchResult!)
        }
        
        return group
    }
    
    private func reAddNewGroup(name:String, place:String, fetchResult:[AutorizationCoreData]) -> GroupsModel? {
        
        var group:GroupsModel?
        let semaphore = DispatchSemaphore (value: 0)

        let parameters = "parent_folder_id=\(place)&name=\(name)"
        let postData =  parameters.data(using: .utf8)
        let auth = autorizationClass.auth(refreshtoken: fetchResult[0].refreshToken!)
        
        var request = URLRequest(url: URL(string: "http://macbook-pro-admimn.local:3000/api/v1/testings/folders")!,timeoutInterval: Double.infinity)
        
        
        if (auth) != nil{
            request.addValue("Bearer \(auth!.access_token)", forHTTPHeaderField: "Authorization")
        }else{
            request.addValue("Bearer \(fetchResult[0].accessToken!)", forHTTPHeaderField: "Authorization")
        }
        
        request.addValue("application/x-www-form--urlencoded", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            
            group = try? JSONDecoder().decode(GroupsModel.self, from: data)
            print(group)
            
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        
        return group
    }
    
}
