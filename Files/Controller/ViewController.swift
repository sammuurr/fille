//
//  ViewController.swift
//  Files
//
//  Created by ADMIMN on 20.01.2021.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    
    @IBOutlet var loginTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    let autorizationClass = AutoriztionModel()
    let viewContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.global(qos: .userInteractive).async { [self] in

            let fetch = NSFetchRequest<AutorizationCoreData>(entityName: "AutorizationCoreData")
            let sortInfo = NSSortDescriptor(key: "tokenTimeOut", ascending: false)
            fetch.sortDescriptors = [sortInfo]
            let fetchResult = try? viewContext.fetch(fetch)
            
            if fetchResult?[0].refreshToken != nil{
                let sesion = AutoriztionModel().auth(refreshtoken: fetchResult![0].refreshToken!)
                
                if sesion != nil {
                    let vc = storyboard?.instantiateViewController(withIdentifier: "main")
                    vc!.modalPresentationStyle = .fullScreen
                    auth = sesion
                    
                    DispatchQueue.main.async {
                        self.present(vc!, animated: true, completion: nil)
                    }
                }
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    func (<#parameters#>) -> <#return type#> {
        <#function body#>
    }

    @IBAction func removeKeyboard(_ sender: Any) {
        
        view.endEditing(true)
        
    }
    
    
    @IBAction func loginTextFieldDelegate(_ sender: UITextField) {
        
        passwordTextField.becomeFirstResponder()
        
    }
    
    
    @IBAction func passwordTextFielsDelegate(_ sender: UITextField) {
        
        view.endEditing(true)
        
    }
    
    @IBAction func siginButton(_ sender: UIButton) {
        
        if loginTextField.hasText, passwordTextField.hasText{
            let sesion = autorizationClass.sigin(login: loginTextField.text!, password: passwordTextField.text!)
            
            if sesion == nil{
                let alert = UIAlertController(title: "Ошибка Авторизации", message: "Логин или пароль введены не правильно. Попробуйте еще раз", preferredStyle: .alert)
                
                let action = UIAlertAction(title: "Ok", style: .cancel)
                alert.addAction(action)
                
                self.present(alert, animated: true) { [self] in
                    
                    loginTextField.text = ""
                    passwordTextField.text = ""
                    loginTextField.becomeFirstResponder()
                }
                
            }else {
                
                let vc = storyboard?.instantiateViewController(withIdentifier: "main")
                vc!.modalPresentationStyle = .fullScreen
                auth = sesion
                
                self.present(vc!, animated: true, completion: nil)
                
            }
            
        }
    
        
    }
    
}

