//
//  GroupCollectionViewController.swift
//  Files
//
//  Created by ADMIMN on 21.01.2021.
//

import UIKit


var auth: Autorization?

class GroupCollectionViewController: UICollectionViewController {
    
    @IBOutlet var backButtonItem: UIBarButtonItem!
    private var groups:[GroupsModel]?
    private var parentId:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        groups = Groups().fetchGroups(auth: auth, folder_id: nil)
    }



    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return groups?.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "group", for: indexPath) as! GroupsCollectionViewCell
    
        if indexPath.item == 0{
            if groups![0].parent_folder_id != nil{
                backButtonItem.isEnabled = true
            }else{
                backButtonItem.isEnabled = false
            }
        }
        
        cell.nameLabel.text = groups?[indexPath.item].name ?? "NoName"
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let group = groups![indexPath.item]
        
        
        parentId.append(group.parent_folder_id ?? "")
        
        groups = Groups().fetchGroups(auth: auth, folder_id: group.id)
        collectionView.reloadData()
    }

    @IBAction func backBarButton(_ sender: UIBarButtonItem) {

        if parentId.last != nil {

            
            groups = Groups().fetchGroups(auth: auth, folder_id: parentId.last!)
            if parentId[0] != parentId.last{
                parentId.removeLast()
            }
            
            collectionView.reloadData()
            
        }
        
    }
    
    @IBAction func addGroupsFile(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Create new group", message: "Please enter the name of the new group", preferredStyle: .alert)
        let alertButton = UIAlertAction(title: "Break", style: .destructive )
        
        let alertButton2 = UIAlertAction(title: "Create", style: .cancel) { [self] (alertAction) in
            var nameText = alert.textFields?[0].text
            if nameText == nil, nameText == ""{
                nameText = "NoName"
            }
            
            
            if parentId.last != nil, parentId.last != "" {
                let addingNewElement = Groups().addNewGroup(name: nameText!, place: parentId.last!)
                
                
                if addingNewElement != nil {
                    groups = Groups().fetchGroups(auth: auth, folder_id: nil)
                    collectionView.reloadData()
                }else{
                    let alert = UIAlertController(title: "Create error", message: "An error occured while adding a group", preferredStyle: .alert)
                    let alertButton = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    alert.addAction(alertButton)
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                let addingNewElement = Groups().addNewGroup(name: nameText!, place: "")
                
                
                if addingNewElement != nil {
                    groups = Groups().fetchGroups(auth: auth, folder_id: nil)
                    collectionView.reloadData()
                }else{
                    let alert = UIAlertController(title: "Create error", message: "An error occured while adding a group", preferredStyle: .alert)
                    let alertButton = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                    alert.addAction(alertButton)
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        alert.addAction(alertButton2)
        alert.addAction(alertButton)
        
        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Group name"
        })
        
        self.present(alert, animated: true)
        
    }
    
    
}
